<?php

function parseCsv($dir)
{
    $data = [];

    // Check if the directory exists
    if (!is_dir($dir)) {
        return "Directory not found";
    }

    // Loop through all files in the directory
    $files = scandir($dir);
    $files = array_diff(scandir($dir), array('.', '..'));
    foreach ($files as $file) {
        if ($file !== '.' && $file !== '..') {
            $csvFile = $dir . '/' . $file;

            // Read the CSV file
            if (($handle = fopen($csvFile, "r")) !== FALSE) {
                $header = null; // Initialize header to null
                while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
                    // Skip rows with no data
                    if (array_filter($row)) {
                        if ($header === null) {
                            $header = $row; // Set header if it's not set
                        } else {
                            $data[] = array_combine($header, $row); // Combine header and row into associative array
                        }
                    }
                }
                fclose($handle);
            } else {
                return "Error reading file: " . $file;
            }
        }
    }

    $data = openAIQuery("Birth of jesus", $data);
    return $data;
}




$dir = "Beginner";

echo "<pre>";
var_dump(parseCsv($dir));





function openAIQuery($topic,$content)
{
    $api_url = 'https://api.openai.com/v1/chat/completions';
    $api_key = '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';
    $format = 'Lesson Title: [Title of the lesson here ]

Objectives:
[Objectives here]

Words to Know:
[List of words to know here]

Materials Needed:
[List of material needed here]

Preparation:
[Preparation content here]

How to Teach This Lesson:
[how to teach this lesson content here]

Review Questions:
[Review questions here]

Activity:
[List of activities here]

if no data found return "False"
';

    $data = [
        "model" => "gpt-4-turbo-preview",
        "messages" => [
            ["role" => "system", "content" => "You are a json data reader"],
            ["role" => "user", "content" => "I want you to find a lesson from the provided data set. lesson must be belong to this topic $topic.Output formate must be similar to this $format."],
            ["role" => "assistant", "content" => json_encode($content)],
        ],
    ];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $api_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
    curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json", "Authorization: Bearer $api_key"]);
    $response = curl_exec($ch);
    if ($response === false) {
        echo 'cURL error: ' . curl_error($ch);
        return [];
    }
    curl_close($ch);
    $result = json_decode($response, true);
    
    return $result;
}



